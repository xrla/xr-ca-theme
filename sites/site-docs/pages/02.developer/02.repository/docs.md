---
title: Interacting With The Repository
taxonomy:
    category: docs
---

[Repository](https://gitlab.com/xrla/xrla-user-directory)  
The repository stores the CRM's `/user` directory which holds all site changes. All alterations to the site are autmatically synced back to the repositories `master` branch.  This repository holds confidential information and should be protected with this in mind.  

## Branching Pattern

`master` branch is a complete mirror of the production deployment and source for `hotfix` branches.  
`develop` branch is a the main source branch for `release`, `feature`, `patch` branches.  
`hotfix` branches are merged off of `master` branch and merged back into `master` branch.  

### Branch Definitions

`master`<sup>1</sup> production site.  
`develop`<sup>1</sup> mainline development branch where all `realease` and `feature` code is merged into and tested before merging into `master`.  
`release` branches encompass a full `major`.`minor` release often with a package of tested features & patches.  
`feature` branches encompass a single feature often being developed by a single person or team.  
`hotfix` branches patch bugs that result in degradation of the site and need to be patched with a high priority.  
`patch` branches patch bugs in the software that don't require the immediacy of a `hotfix`.  

<em><sup>1</sup> Protected branches meaning pushing to this repository is limits to senior developers. What you can do if your aren't a senior developer is do a merge request within Gitlab. </em>

## Versioning Pattern

`MAJOR`.`MINOR`.`PATCH`  

* **MAJOR** version changes to Grav themes or plugins that form breaking feature changes  
* **MINOR** version changes to Grav themes or plugins that are still backwards-compatible with previous versions  
* **HOTFIX PATCH** version changes based on hotfix patches  

