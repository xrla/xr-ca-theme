---
title: Hero
hide_git_sync_repo_link: false
menu: Top
hero_classes: text-center
hero_image: xrcal-slide.gif
hero_portrait_image: xrcal-slide-mobile.gif
banner_above:
  classes: bg-green text-center
  text: Extinction Rebellion California
---
