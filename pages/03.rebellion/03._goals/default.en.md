---
title: "Goals"
classes: "bg-lightgrey"
---

# XRCAL's Fundamental Goals{.red-line}

XRCAL holds dear the tenets of the international XR group<sup><a href="#footnote2">1</a></sup> with a California focus. In sum these are that XRCAL is an ecological activist group intent on creating a world fit for healthy habitation by generations to come, free of the existential dangers of the consequences of global warming. XRCAL's mission to concentrate on what is necessary to achieve this goal is one grounded in focused non-violent activism and civil disobedience in the service of mobilizing a broad coalition of a minimum of 3.5% of California's population (355,750 people) ready to be available for regular and repeated actions in the service of achieving XRCAL's goals. XRCAL is part of a national XR goal of raising 3.5% or more of the U.S. Population (11,497,200 people) to fight to change the inhumanity of today's climate and environment- destroying norms.

<a name="footnote2"></a>

<ul class="footnotes">
<li><em><sup>1</sup><a href="https://risingup.org.uk/about">https://risingup.org.uk/about</a></em></li>
</ul>
