---
title: "Ideology"
classes: "bg-blue"
---

# XRCAL's Ideology{.red-line}

XRCAL intends to spark and sustain a spirit of collective, creative rebellion which will enable much needed changes in our political, economic, and social landscape. XRCAL is an alliance of California XR cells and seeks to change the national consciousness to become one based on conservation of our ecosystems. XRCAL believes in fostering cooperation within our citizenry to quickly reach the goal of living sustainably within our natural environment _as a part of it, not apart from it_. We work to transform our society into one that is compassionate, inclusive, kind, sustainable, equitable, and interpersonally connected. Additionally we endeavor to work together to mobilize and train local organizers in California to work with their local communities to develop, using the XR framework, tools they need to address California's deeply rooted problems as they relate to the threats created by global warming.

XRCAL is allied with the broadly scientifically accepted understanding that achieving average global temperatures of greater than or equal to a 1.5&deg;C increase over preindustrial averages will be the cause of severe, adverse, and consequential impacts on life as humanity understands it today<sup><a href="#footnote1">1</a></sup> and that an increase of 2&deg;C or greater will have catastrophic and perhaps existential consequences for the human species and all of the earth's flora and fauna.<sup><a href="#footnote1">2</a></sup> XRCAL acknowledges that this is reality; not a myth, a joke, or power grab by nefarious forces, but a dangerous future that we living today and those that might follow us will, _without exception_, inhabit should we continue with “business as usual”.

Maintaining +1.5&deg;C is our best hope and it will be difficult to achieve — the window to reach this benchmark grows smaller every day. _This goal will require sacrifice by every Angeleno_. The consequences of failing are real, terrifying, and will mean an end to life on earth as we understand it today.<sup><a href="#footnote1">3</a></sup> XRCAL understands that this message is not popular, nor one people wish to hear. It is a message that inspires despair and grief and XRCAL maintains a goal of acknowledging this grief and is allied with local 24 organizations able to help Californians reconcile this grief and turn it into positive action in the service of the future of our earth and all of its inhabitants. For better or worse we all are living through a moment of consequence like which humankind has never seen before. We must rise to meet it.

<a name="footnote1"></a>

<ul class="footnotes">
<li><em><sup>1</sup><a href="https://www.un.org/sustainabledevelopment/blog/2018/10/special-climate-report-1-5oc-is-possible-but-requires-unprecedented-and-urgent-action/">https://www.un.org/sustainabledevelopment/blog/2018/10/special-climate-report-1-5oc-is-possible-but-requires-unprecedented-and-urgent-action/</a></em></li>
<li><em><sup>2</sup><a href="https://www.wri.org/blog/2018/10/half-degree-and-world-apart-difference-climate-impacts-between-15-c-and-2-c-warming">https://www.wri.org/blog/2018/10/half-degree-and-world-apart-difference-climate-impacts-between-15-c-and-2-c-warming</a></em></li>
<li><em><sup>3</sup><a href="http://nymag.com/intelligencer/2017/07/climate-change-earth-too-hot-for-humans.html">http://nymag.com/intelligencer/2017/07/climate-change-earth-too-hot-for-humans.html</a></em></li>
</ul>
