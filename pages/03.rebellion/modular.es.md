---
title: La Rebelion
content:
    items: '@self.modular'
published: true
hide_git_sync_repo_link: true
hero_classes: 'text-light' 
onpage_menu: true
hero_image: unsplash-text.jpg
background:
    parallax: true
simplehero: true 
action_network_form_id: xr-la-sign-up
---
