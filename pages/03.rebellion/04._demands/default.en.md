---
title: Demands
classes: modular-demands-full
---

# Demands of XRCAL{.red-line}

[plugin:page-inject](/rebellion/_demands/_01)
[plugin:page-inject](/rebellion/_demands/_02)
[plugin:page-inject](/rebellion/_demands/_03)
[plugin:page-inject](/rebellion/_demands/_04)

<a name="footnote3"></a>

<ul class="footnotes">
<li><em><sup>1</sup></em> <a href="https://medium.com/@AlexSteffen/predatory-delay-and-the-rights-of-future-generations-69b06094a16">https://medium.com/@AlexSteffen/predatory-delay-and-the-rights-of-future-generations-69b06094a16</a></em></li>
<li><em><sup>2</sup></em> <a href="https://theenergymix.com/2018/11/26/extinction-rebellion-goes-global-with-call-for-net-zero-emission-by-2025/">https://theenergymix.com/2018/11/26/extinction-rebellion-goes-global-with-call-for-net-zero-emission-by-2025/</a></em></li>
</ul>
