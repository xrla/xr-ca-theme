---
title: '03'
class: 'underline yellow-line'
---

# Citizens' Assembly{.red-line .inline-block #citizens-assembly}

The creation of County and Municipal Citizens' Assemblies legally empowered to oversee the changes required above, as part of creating a democracy fit for the purpose of arresting global warming and empowering citizens over an establishment that has delivered us to this dangerous climate threshold. These Assemblies shall operate from a science and evidence-based position to ensure that Los Angeles is able to meet or exceed its goal of County-wide net zero carbon emissions by 2025.