---
title: "01"
class: "underline yellow-line"
---

# Tell the truth{.red-line .inline-block #tell-the-truth}

That California governments from the State to the County to the Municipal level must tell the truth about global warming and the wider ecological emergency we have created without departmental exception. These governments and their departments must reverse policies inconsistent with addressing these issues and work with the media to communicate to citizens the dangers we face and the changes that must be made to mitigate future impacts of global warming on California's population and natural environment. California's governments at any level must not be perpetuators of
predatory delay.<sup><a href="#footnote3">1</a></sup>
