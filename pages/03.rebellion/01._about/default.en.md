---
title: "About"
---

# What The Hell Is Extinction Rebellion California?{.red-line}

Extinction Rebellion California (XRCAL) is an alliance of California cells in the international Extinction Rebellion organism which made its debut in London in October of 2018 after being incubated for nearly two years by Rising UP!, a direct action non-violent activist incubator. As of May Day 2019 there are nearly 400 cells on five continents — an incredible spread of a social movement by any metric.

![Extinction Rebellion Butterfly](xrla-hourglass-butterfly-logo.svg?resize=200,200){.float-left}
Extinction Rebellion California aims to help organize XR cells throughout California to be more effective and work in concert to achieve the broader aims of Extinction Rebellion here in the state. We have published the first edition of our statewide newspaper, The Rebellion Recorder, a real live newsprint broadsheet distributed throughout California. XR groups all over the state have undertaken actions to influence California's governments to treat the climate crisis as if it were a real crisis, not a business opportunity or something else to be addressed by status quo behaviors. Find a group near you on our website [xrcal.org](www.xrcal.org).

XRCAL has as its goals the overarching goals of the international movement but with a California focus. This can be distilled to:

![Extinction Rebellion Hand Pointing](xr-hourglass-hand-pointing.svg?resize=200,200){.float-right}
_We in XRCAL aim to jolt 3.5% of Californians out of their ordinary lives to actively join us in demanding realistic and timely global warming action by our governments to stave off the worst effects of global warming that climate science tells us are coming, soon. We will do this by acts of non-violent civil disobedience, negotiation, education, and the supporting of and working with existing local groups working at the intersections of ecological and climate justice, social justice, and any other local group who sees our current climate reality for what it is — an existential threat to life on earth as we have come to know it._

Our climate situation is dire and with only about nine years effectively remaining to maintain a +1.5&deg;C average increase over preindustrial historical mean atmospheric temperatures we are running out of time. Fast. The dissembling of industry, government, and advocacy groups that has led us to this dire place will not stand. Extinction Rebellion is a people’s movement and will not be co-opted. Our continued survival on earth demands we succeed, the consequences of failure are not acceptable.

Join us! For the future; for your future, your children's future, the earth's future. You get the idea...

<div class="text-center"><img src="/user/themes/xrla/images/sabertooth-skull.svg" height="80" /> <strong class="h2" style="vertical-align: top;top: 9px;position: relative;">OUR FUTURE WILL BE WON BY DEGREES</strong> <img src="/user/themes/xrla/images/human-skull.svg" height="80" /></div>
