---
title: 1.1°C
subtitle: "Humans have raised the planet’s temperature 1.1°C"
external_link: 
--- 

Over 50% of the human addition of carbon to the atmosphere through the burning of fossil fuels has occurred in the last 25 years – i.e. since the IPCC was founded.

Human activities have caused the planet's average surface temperature to rise about 1.1&deg;C since the late 19th century. Most of the warming occurred in the past 35 years. 

![Global Mean Temperature](global-mean-temperature.png)