---
title: 9 million
subtitle: "In 2015 all forms of pollution were responsible for an estimated 9 million deaths"
external_link: 
internal_link:  
--- 

All forms of pollution were responsible in 2015 for an estimated 9 million premature deaths—16% of all deaths worldwide—as well as for 268 million disability-adjusted life-years. Pollution is thus the world’s largest environmental cause of disease and premature death.

As the world gets hotter and more crowded, our engines continue to pump out dirty emissions, and half the world has no access to clean fuels or technologies (e.g. stoves, lamps), the very air we breathe is growing dangerously polluted: nine out of ten people now breathe polluted air, which kills 7 million people every year. (Ambient air pollution: 4.2 million deaths; household air pollution: 2.8 million deaths)