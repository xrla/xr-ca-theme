---
title: 'Emergency'
content:
    items: '@self.children'
    limit: 0 
classes: bg-pink
---

# The Emergency{.purple-line .text-center}

<blockquote>"We are in a planetary emergency" - <cite>Prof. James Hansen, former Director of the NASA Goddard Institute for Space Studies</cite></blockquote>

<blockquote>"Climate change is a medical emergency…It thus demands an emergency response…" - <cite>Prof. Hugh Montgomery, director of the University College London Institute for Human Health and Performance, Lancet Commission Co-Chair</cite></blockquote>

<blockquote>"This is an emergency and for emergency situations we need emergency action." – <cite>Ban Ki-Moon, former UN Secretary General</cite></blockquote>

<blockquote>"Act now to save our planet and our future from the climate emergency." - <cite>Antonio Guterres UN Secretary-General</cite></blockquote>

## Warnings{.black-line .text-center}

At the end of 2018, the UN Secretary General warned us:

* Humanity and life on Earth now face a 'direct existential threat'
* The world must act swiftly and robustly to keep global warming under 1.5&deg;C and try to avoid utterly catastrophic impacts to life on Earth.

Human activity is causing irreparable harm to the life on this world. A mass extinction event, only the sixth in roughly 540 million years, is underway. Many current life forms could be annihilated or at least committed to extinction by the end of this century.

The air we breathe, the water we drink, the earth we plant in, the food we eat, and the beauty and diversity of nature that nourishes our psychological well-being, all are being corrupted and compromised by the political and economic systems that promote and support our modern, consumer-focussed lifestyles.

We must act while we still can. What we are seeing now is nothing compared to what could come.  

Effects on global human society, if the climate and ecological emergency is not addressed, may spiral out of control.

* Sea level rise
* Desertification
* Wildfires
* Water shortage
* Crop failure
* Extreme weather
* Millions displaced
* Disease
* Increased risk of wars and conflicts

But our leaders are failing in their duty to act on our behalf. Our current systems of governance is compromised by a focus on profits and economic growth. Politicians can be influenced by lobbies of powerful corporations and the media are hampered by vested interest of corporate advertisers undermining our democratic values.

We have run out of the luxury of time to react incrementally..

We must radically and immediately begin reducing emissions and improving carbon absorption, drawing it down and locking it up again.

Only a peaceful planet-wide mobilisation of the scale of World War II will give us a chance to avoid the worst case scenarios and restore a safe climate

The task before us is daunting but big changes have happened before.

Let’s make a better world.

## The Crises{.black-line .text-center}