---
title: 50%
subtitle: "50% of the planet’s topsoil has been lost in the last 150 years"
external_link: 
internal_link:  
--- 

More than 95% of what we eat comes from soil. It takes about 500 years to form 2.5 cm of top soil under normal agricultural condition.

Soil erosion and degradation has been increased dramatically by the human activities of deforestation for agriculture, overgrazing and use of agrochemicals.

50% of the planet’s topsoil has been lost in the last 150 years, leading to increased pollution, flooding and desertification. Desertification itself  currently affects more than 2.7 billion people