---
title: 130 million
subtitle: "2°C warming  would threaten to inundate areas now occupied by 130 million people"
external_link: 
internal_link:  
--- 

Sea level is rising faster in recent decades. Sea level rise is caused primarily by two factors related to global warming: the added water from melting ice sheets and glaciers and the expansion of seawater as it warms. Sea level rises will cause inundation of low lying land, islands and coastal cities globally. 

As sea level rises higher over the next 15 to 30 years, tidal flooding is expected to occur much more often, causing severe disruption to coastal communities, and even rendering some areas unusable — all within the time frame of a typical home mortgage.