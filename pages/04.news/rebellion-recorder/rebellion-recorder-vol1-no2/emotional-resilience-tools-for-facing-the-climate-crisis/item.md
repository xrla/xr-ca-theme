---
title: "Emotional Resilience: Tools for Facing the Climate Crisis"
subtitle: Grief Is Common When Facing Global Warming
author: Mary Robins
date: "00:00 10/07/2019"
show_sidebar: false
show_breadcrumbs: true
blog_url: /news
continue_link: true
taxonomy:
  category: Rebellion Recorder
  author: Mary Robins
  tag:
    - Mental Health
    - Resilience
# hero_image: cnn-offices.jpg
# body_classes: overlay-light
# hero_caption: "CNN officials stop photography of an action by SoCal350, Climate Strike LA, and XRLA at their Los Angeles offices."
# header_image_credit: J. Matt
---

Mary Robins — Writer
In 2018, the global wellness industry brought in \$4.2 tril- lion — more than the US gov- ernment collected in income and payroll taxes. Humans, it seems, are clamoring for in- ner peace.
And little wonder, facing as we are the growing possibil- ity of extinction. Fear, anger, grief, and anxiety are ratio- nal, healthy responses to the current state of life on earth, though they’ve been patholo- gized by a culture that prizes personal peace above all else. These strong emotions are helpful, too: in their potency, we can find the strength we need to act. But how can we make space for these feelings, and even harness them, with- out burning out?
Grief
Most people report a period of grieving upon stepping into climate reality. This grief comes from love for this planet, and it’s import- ant to allow it — for example by seeking out climate grief groups. In her forthcom- ing book, Facing the Climate Emergency: How to Transform Yourself with Climate Truth, former therapist and now executive director of the Cli- mate Mobilization Margaret Klein Salamon writes: “If we stop ourselves from feeling grief, we stop ourselves from processing the reality of our loss. If we can’t process our loss, then we can’t live in re- ality. We become imprisoned and immobile.”
If we embrace our grief it can become our greatest motiva- tor. “It’s a fuel to stay work- ing,” Nicole Marie, who runs grief groups for XRLA, told me. “The more I open to it, the more energy I have to volunteer and to do actions, to find my own bravery... about what I would be willing to do for the planet.”
As we grieve, Salamon told me, it’s important to mourn our own imagined futures. “All of your lovely plans for your career and family and retirement, and how you had hoped it would play out — it’s not going to happen. And recognizing that is really im- portant and freeing, because it allows you to create a dif- ferent sense of self and idea for the future, which is in line with reality.”
Experts agree: action is the single best coping mech- anism. Activism creates a sense of agency and hope. As Salamon told me, once you’ve grieved for your own lost fu- ture, you find that each one of us is called to be a hero of this moment, in our own way.
As a bonus, the kind of non- violent direct action with which XR has had such suc- cess around the world brings a sense of creativity and play — both healing antidotes to the thoughtless march to- ward apocalypse. There can even be joy in this space of truth and love, dire as the re- alities are.
Community
Activism also breaks isolation by bringing you into com- munity with others living in climate reality. Living in cli- mate truth in a world in deep denial can feel profoundly alienating. It’s essential to find others who know the full truth and are acting like it matters. A supportive net- work of fellow activists is also a powerful bulwark against disengaging.
Self-Compassion
Difficult feelings will keep arising. Honor each as a healthy response to the sit- uation. And sometimes, self-compassion means giv- ing yourself a break — focus on something restorative for a while, and come back re- freshed. Activists we consult- ed most commonly recom- mended exercise, meditation, and spending time in nature or with children.
Know This
Living this truth is hard. Thank you for doing it. We see you and are so grateful.

_— Mary Robins is a newpaper and magazine writer residing in Los Angeles._
