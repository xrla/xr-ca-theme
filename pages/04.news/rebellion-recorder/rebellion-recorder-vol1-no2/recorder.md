---
title: "Rebellion Recorder Fall 2019"
subtitle: "Vol1 No2"
date: "00:00 10/07/2019"
content:
  items: "@self.children"
  order:
    by: folder
    dir: asc
  limit: 0
  taxonomy:
    category: Rebellion Recorder
    type: Newsletter
    tag:
      - newsletter
      - recorder
show_sidebar: false
bricklayer_layout: true
show_breadcrumbs: true
continue_link: true
hero_image: masthead.jpg
body_classes: hero-contain overlay-light hero-narrow
---

Articles by John Pfenning, Alycee Lane, Carmiel Banasky, Jesus Villalba, Jesus Villalba, Mary Robins, Mary Robins, Ami Chen Mills-Naim, Magick Altman

===

[pdfjs file=RebellionRecorderFall2019.pdf height=900]
